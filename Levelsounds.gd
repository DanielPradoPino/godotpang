extends AudioStreamPlayer

var sounds
var soundsArray
var sample_library={"music1":preload("res://assets/music/music1.ogg"),
				"leveldone":preload("res://assets/music/levelDone.ogg"),
				"ready":preload("res://assets/sounds/readyvoice.ogg"),
				"letsgo":preload("res://assets/sounds/letsgo.ogg"),
				"timeping":preload("res://assets/sounds/timepings.ogg"),
				"buttonpush":preload("res://assets/sounds/buttonPush.ogg"),
				"extraLive":preload("res://assets/sounds/extraLife.ogg")}
	

func playSound(sound,pitch):
	
	if sample_library.has(sound):
		stop()
		if !is_playing():
			stream = sample_library[sound]
			set_pitch_scale(1)
			play(0.0)
