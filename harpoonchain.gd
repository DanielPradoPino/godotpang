extends Area2D

func _ready():
	if globals.playerName=="Ivan":
		modulate=Color("2b85ad")

func _on_HarpoonChain_body_entered(body):
	if("Bubble" in body.name):
		body.die()
		globals.canShot=true
		get_parent().queue_free()
	elif body.is_in_group("platforms"):
		queue_free()
