extends Area2D

var start=false
var speed=2*globals.speedIncrease
var n=0
var t1=0
var t2=0
var t3=0
var playing=false
var step=1
var left=true
var preAnimName=""

func _ready():
	if $Label.text=="left":
		left=true
	else:
		left=false
	if !globals.playerName=="Nayra":
		preAnimName=globals.playerName



func _process(delta):
	if start:
		if step==1:
			$Nayra.play("%srun"%preAnimName)
			if !left:
				if $Nayra.global_position.x<$Position2D.global_position.x:
					$Nayra.global_position.x+=speed
				else:
					step+=1
			else:
				if $Nayra.global_position.x>$Position2D.global_position.x:
					$Nayra.global_position.x-=speed
				else:
					step+=1
		if step==2:
			$Nayra.play("%sidle"%preAnimName)
			$textBubble.visible=true
			if n<1:
				n+=0.01
				$textBubble.modulate=Color(1,1,1,n)
			else:
				step+=1
		if step==3:
			$text1.visible=true
			if t1<1:
				t1+=0.003
				$text1.modulate=Color(1,0.4,1,t1)
			else:
				step+=1
		if step==4:
			$text1.visible=false
			$text2.visible=true
			if t2<1:
				t2+=0.003
				$text2.modulate=Color(1,0.4,1,t2)
			else:
				step+=1
		if step==5:
			$text2.visible=false
			$text3.visible=true
			if t3<1:
				t3+=0.003
				$text3.modulate=Color(1,0.4,1,t3)
				playing=false
			else:
				step+=1
		if step==6:
			$text3.visible=false
			$textBubble.visible=false
			$Nayra.play("%srun"%preAnimName)
			if !left:
				if $Nayra.global_position.x<1200:
					$Nayra.global_position.x+=speed
				else:
					get_node("../").finished=true
			else:
				if $Nayra.global_position.x>-200:
					$Nayra.global_position.x-=speed
				else:
					get_node("../").finished=true
		if step==101:
			get_node("../sounds").playSound("flyby",1)
			if $airCraft.global_position.x>-600:
				$airCraft.global_position.x-=speed
			else:
				$airCraft.visible=false
				get_node("../sounds").stop()
				step=1
