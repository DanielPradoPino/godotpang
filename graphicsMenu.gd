extends Node2D
var resolution="800x600"
var fullScreen=false
var borderless=false
var vsync=true
var file=File.new()

func _ready():
	$menuMusic.play(globals.menuMusicPosition)
	for resolution in globals.resolutions:
		$ui/control/Resolution.add_item(resolution)
	if file.open_encrypted_with_pass(globals.screenFile, File.READ,"Nayra") != 0:
		pass
	else:
		var data = file.get_line().split(",")
		file.close()
		$ui/control/Resolution.selected=int(data[0])
		resolution=$ui/control/Resolution.text
		$ui/control/fullScreen.pressed=bool(int(data[1]))
		$ui/control/borderless.pressed=bool(int(data[2]))

func _input(event):
	if event is InputEventMouseButton and event.is_pressed():
		if $ui/control/Back/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			globals.menuMusicPosition=$menuMusic.get_playback_position()
			globals.goto_scene("optionsMenu")
		if $ui/control/apply/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			applyChanges()
	if $ui/control/fullScreen.pressed:
		$ui/control/borderless.pressed=false
		$ui/control/borderless.disabled=true
	else:
		$ui/control/borderless.disabled=false


func applyChanges():
	$menuSounds.playSound("plop",1)
	resolution=$ui/control/Resolution.selected
	fullScreen=int($ui/control/fullScreen.pressed)
	borderless=int($ui/control/borderless.pressed)
	var data = "%d,%d,%d"%[resolution,fullScreen,borderless]
	if file.open_encrypted_with_pass(globals.screenFile, File.WRITE,"Nayra") != 0:
		print("Error saving data")
	else:
		file.store_line(data)
		file.close()
	var resSize=$ui/control/Resolution.text.split("x")
	OS.window_fullscreen=fullScreen
	OS.window_size=Vector2(int(resSize[0]),int(resSize[1]))
	OS.window_borderless=borderless
	OS.vsync_enabled=vsync

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		# For Windows
		pass        
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: 
        # For android
		globals.menuMusicPosition=$menuMusic.get_playback_position()
		globals.goto_scene("optionsMenu")
