extends Node

var resolutions=["800x600","1024x600","1280x720","1920x1080"]
var handicap=1
var current_scene = null
var menuMusicPosition=0
var speedIncrease=1
var mobile=false
var demo=false
var boss=false
var onScreenBalls=0
var ts=OS.get_unix_time()
var level="Level1"
var levelCount=1
var winds=0
var portals=0
var canShot=true
var maxShot=1
var maxOnscreenItems=2
var onscreenItems=0
var shots=0
var lives=3
var canDie=false
var score=0
var gamePaused=false
var dynamite=false
var pause_menu
var saveFile="user://save.sav"
var langFile="user://lang.sav"
var screenFile="user://screen.sav"
var playerFile="user://playerData.sav"
var saveImg="user://saveData.png"
var saveData={}
var map=false
var onArea=false
var movePlatforms=false
var extraLives=0
var extraLifeAt=2000
var cutScn=false
var player
var playerName
const NayraPlayer=preload("res://Nayra.tscn")
const IvanPlayer=preload("res://Ivan.tscn")
const biggerBubble = preload("res://BiggerBubble.tscn")
const bigBubble = preload("res://BigBubble.tscn")
const mediumBubble = preload("res://MediumBubble.tscn")
const smallBubble = preload("res://SmallBubble.tscn")
const header = preload("res://Header.tscn")
const readyScn = preload("res://ready.tscn")
const pauseMenu = preload("res://PauseMenu.tscn")
const item = preload("res://Item.tscn")
const analog = preload("res://analog.tscn")


func reload():
	levelCount=1
	canShot=true
	lives=3
	canDie=false
	score=0
	gamePaused=false
	shots=0

func continueGame():
	canShot=true
	canDie=false
	gamePaused=false

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)
	checkSaveConfig()
	

func checkSaveConfig():
	var file=File.new()
	if file.open_encrypted_with_pass(globals.screenFile, File.READ,"Nayra") != 0:
		pass
	else:
		var data = file.get_line().split(",")
		file.close()
		var resSize=resolutions[int(data[0])].split("x")
		OS.window_fullscreen=bool(int(data[1]))
		OS.window_size=Vector2(int(resSize[0]),int(resSize[1]))
		OS.window_borderless=bool(int(data[2]))
		OS.vsync_enabled=true;
	if file.open_encrypted_with_pass(globals.playerFile, File.READ,"Nayra") != 0:
		player=NayraPlayer
		playerName="Nayra"
	else:
		var data = file.get_line().split(",")
		file.close()
		playerName=data[0]
		if playerName=="Ivan":
			player=IvanPlayer
		elif playerName=="Nayra":
			player=NayraPlayer

func get_level():
	return level
	
func pauseMenu():
	if gamePaused:
		gamePaused=false
		pause_menu.queue_free()
		get_tree().get_root().get_child(1).get_node("Player").pause_mode=2
		get_tree().get_root().get_child(1).timer.pause_mode=2
		get_tree().paused=false
	else:
		gamePaused=true
		get_tree().get_root().get_child(1).get_node("Player").pause_mode=1
		get_tree().get_root().get_child(1).timer.pause_mode=1
		pause_menu=pauseMenu.instance()
		get_tree().get_root().get_child(1).add_child(pause_menu)
		pause_menu.global_position= get_tree().get_root().get_child(1).get_node("Camera2D").get_camera_screen_center()
		pause_menu.pause_mode=2
		get_tree().paused=true
		

func goto_scene(levelName):
   
	var mapLevels=[6,11,16,21,26,31,32]
	var bossLevels=[31,32]
	if levelCount==6 and demo:
		levelName="demo"
	if levelCount==32:
		levelName="Menu"
	if !get_tree().get_root().get_child(1).name=="Menu":
		if levelCount in mapLevels and !map and cutScn:
			map=true
			if !levelCount in bossLevels: 
				levelName="Map"
		if levelCount in mapLevels and !cutScn:
			cutScn=true
			levelName="cutScn1"
	level=levelName
	var path="res://%s.tscn"%levelName
	call_deferred("_deferred_goto_scene", path)



func getCollisionNode(body, body_shape):
	var col_shape = -1
	var node_shape = null
	if !"Bubble" in body.name:
		if(body.get_child_count()>=1):  
			for i in range(body.get_child_count()):
				if body.get_child(i).name.begins_with("CollisionShape2D"):
					col_shape += 1
				if col_shape == body_shape:
					node_shape = body.get_child(i)
					break
	return node_shape

func _deferred_goto_scene(path):
	# Immediately free the current scene,
	# there is no risk here.
	var sounds=get_tree().get_nodes_in_group("sounds")
	for sound in sounds:
		sound.stop()
	current_scene.queue_free()

	# Load new scene.
	var s = ResourceLoader.load(path)

	# Instance the new scene.
	current_scene = s.instance()

	# Add it to the active scene, as child of root.
	get_tree().get_root().add_child(current_scene)

	# Optional, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)
	