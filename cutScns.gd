extends Node

var city1
var city2
var speed=0.2*globals.speedIncrease
var fadeSpeed=0.005
var alpha=1
var faded=false
var rampDown=true
var finished=false
var cutScn="cutScn1"
func _ready():
	if globals.levelCount==6:
		cutScn="cutScn1"
	if globals.levelCount==11:
		cutScn="cutScn2"
	if globals.levelCount==16:
		cutScn="cutScn3"
	if globals.levelCount==21:
		cutScn="cutScn4"
	if globals.levelCount==26:
		cutScn="cutScn5"
	if globals.levelCount==31:
		cutScn="cutScn6"
		get_node("%s"%cutScn).step=101
	if globals.levelCount==32:
		cutScn="cutScn7"
	
	
	
	

func _process(delta):
	get_node("%s"%cutScn).visible=true
	if faded:
		faded=false
		rampDown=false
	else:
		if rampDown:
			alpha-=fadeSpeed
			if alpha>=0.01:
				$diff.modulate=Color(0,0,0,alpha)
			else:
				faded=true
				get_node("%s"%cutScn).start=true
		else:
			if finished:
				alpha+=fadeSpeed
				if alpha<=0.99:
					$diff.modulate=Color(0,0,0,alpha)
				else:
					faded=true
					$music.stop()
					var level_name="Level%d"%globals.levelCount
					globals.goto_scene(level_name)