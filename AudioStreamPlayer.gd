extends AudioStreamPlayer

var sounds
var soundsArray
var sample_library={"walk":preload("res://assets/sounds/jungle_walk.ogg"),
	"jump":preload("res://assets/sounds/jump.ogg"),
	"dead":preload("res://assets/sounds/dead.ogg"),
	"plop":preload("res://assets/sounds/plop.ogg"),
	"ohyeah":preload("res://assets/sounds/ohyeah.ogg")}
	

func playSound(sound,pitch):
	
	if sample_library.has(sound):
		if "walk" in sound:
			if !is_playing():
				stream = sample_library[sound]
				set_pitch_scale(pitch)
				play()
		elif "ohyeah" in sound:
			stop()
			if !is_playing():
				stream = sample_library[sound]
				set_pitch_scale(pitch)
				play()
		else:
			if !is_playing():
				stream = sample_library[sound]
				set_pitch_scale(1)
				play(0.0)

