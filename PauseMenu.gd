extends Node2D

var option =0
var maxOption=1
var minOption=0


func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		$pauseSounds.playSound("bounce",1)
		globals.pauseMenu()

func _input(event):
	if (event is InputEventMouseButton or event is InputEventScreenTouch) and event.is_pressed():
		if $options/continue/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y-600)):
			globals.pauseMenu()
		elif $options/exit/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y-600)):
			get_tree().paused=false
			globals.levelCount=1
			globals.goto_scene("Menu")
			
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		# For Windows
		pass        
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: 
		# For android
		$pauseSounds.playSound("bounce",1)
		globals.pauseMenu()
