extends Node2D

func _ready():
	$menuMusic.play(globals.menuMusicPosition)
	if globals.playerName=="Nayra":
		$Nayra.play("default")
	elif globals.playerName=="Ivan":
		$Ivan.play("default")

func _input(event):
	if event is InputEventMouseButton and event.is_pressed():
		if $ui/control/apply/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			applyChanges()
		elif $ui/control/Back/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			globals.menuMusicPosition=$menuMusic.get_playback_position()
			globals.goto_scene("optionsMenu")
		elif $ui/NayraControl.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			$Nayra.play("default")
			$Ivan.stop()
			globals.playerName="Nayra"
			globals.player=globals.NayraPlayer
		elif $ui/IvanControl.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			$Ivan.play("default")
			$Nayra.stop()
			globals.playerName="Ivan"
			globals.player=globals.IvanPlayer


func applyChanges():
	$menuSounds.playSound("plop",1)
	var data = globals.playerName
	var file=File.new()
	if file.open_encrypted_with_pass(globals.playerFile, File.WRITE,"Nayra") != 0:
		print("Error saving data")
	else:
		file.store_line(data)
		file.close()

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		# For Windows
		pass        
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: 
        # For android
		globals.menuMusicPosition=$menuMusic.get_playback_position()
		globals.goto_scene("optionsMenu")
