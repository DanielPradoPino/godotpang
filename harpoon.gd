extends Area2D

var Speed=5*globals.speedIncrease
var dochain=25
var initialY=0
const chain = preload("res://harpoonchain.tscn")

func _ready():
	initialY=get_node("../../Player").global_position.y+25
	$sounds.playSound("shot",1)
	if globals.playerName=="Ivan":
		modulate=Color("2b85ad")	
	
func _process(delta):
	dochain+=Speed
	global_position.y=global_position.y-Speed
	if dochain>=35:
		dochain=0
		var Chain=chain.instance()
		add_child(Chain)
		Chain.global_position.y= initialY
	if(global_position.y<=-650):
		globals.shots-=1
		globals.canShot=true
		queue_free()

func _on_Harpoon_body_entered(body):
	if(!body.name=="Player" and !body.name=="Boundaries" and !body.name=="Harpoon"):
		globals.shots-=1
		globals.canShot=true
		if body.is_in_group("destroyable"):
			if get_node("../..").started:
				globals.score+=20
				$sounds.playSound("break",1)
				get_node("../../").updateScore()
				if randi() % 10 <=2:
					item()
				body.queue_free()
		else:
			$sounds.playSound("hit",1)
		queue_free()
	if("Bubble" in body.name):
		body.die()
	if("Boss" in body.name):
		body.hit()
		
func item():
	if(globals.onscreenItems<globals.maxOnscreenItems):
		globals.onscreenItems+=1
		var item=globals.item.instance()
		item.global_position=global_position
		get_node("../..").add_child(item)
