extends AudioStreamPlayer

var sounds
var soundsArray
var sample_library={"flyby":preload("res://assets/sounds/flyby.ogg"),
	"hit":preload("res://assets/sounds/platformhit.ogg"),
	"explode":preload("res://assets/sounds/explosion.ogg")}
	

func playSound(sound,pitch):
	
	if sample_library.has(sound):
		stop()
		stream = sample_library[sound]
		set_pitch_scale(pitch)
		play()
