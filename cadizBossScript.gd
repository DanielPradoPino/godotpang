extends StaticBody2D


var Speed=2
var percentage=2
var life=100
var flick=0
var flicked=false
var bubble
var Scale
var bubblecreated=false
var maxOnScreenBalls=0
var dead=false

func _ready():
	maxOnScreenBalls=int($Label.text)

func _process(delta):
	if $Label.text=="left":
		$Sprite.flip_h=true
		global_position.x-=Speed
		if global_position.x<=-2000:
			$Label.text="right"
		if global_position.x==1200:
			$bossSounds.playSound("flyby",1)
	else:
		$Sprite.flip_h=false
		global_position.x+=Speed
		if global_position.x>=2800:
			$Label.text="left"
		if global_position.x==-400:
			$bossSounds.playSound("flyby",1)
	
	if global_position.x>=50 and global_position.x<=750 and globals.onScreenBalls<=maxOnScreenBalls:
		var p=float(randi() % 50)
		if p<=3 and p>1:
			bubble=globals.mediumBubble.instance()
			Scale=3
			bubblecreated=true
		elif p<=1 and p>0.08:
			bubble=globals.bigBubble.instance()
			Scale=2.2
			bubblecreated=true
		elif p<0.08:
			bubble=globals.biggerBubble.instance()
			Scale=1
			bubblecreated=true
		if bubblecreated:
			bubblecreated=false
			var color=Color(float(randi()%9)/10,float(randi()%9)/10,float(randi()%9)/10,0.8)
			bubble.global_position=global_position
			bubble.modulate=color
			bubble.get_node("Sprite").scale=Vector2(bubble.get_node("Sprite").scale.x/(1.5*Scale),bubble.get_node("Sprite").scale.y/(1.5*Scale))
			bubble.get_node("collision").scale=Vector2(bubble.get_node("collision").scale.x/(1.5*Scale),bubble.get_node("collision").scale.x/(1.5*Scale))
			get_node("../../Pumps").add_child(bubble)
	
	if life<=30:
		flick+=1
		if flick>=life*10:
			modulate=Color(1,0,0,1)
			flicked=true
			flick=0
		if flick>=30 and flicked:
			modulate=Color(1,1,1,1)
			flicked=false
			flick=0
	
	if $Sprite.frame == $Sprite.frames.get_frame_count("dead")-1 and dead:
		queue_free()

func hit():
	life-=2
	$bossFX.playSound("hit",1)
	modulate=Color(1,0,0,1)
	yield(get_tree().create_timer(0.1), "timeout")
	modulate=Color(1,1,1,1)
	globals.score+=20
	get_node("../../").updateScore()
	if life<=0:
		die()

func die():
	globals.boss=false
	globals.score+=2000
	get_node("../../").updateScore()
	$bossSounds.stop()
	$bossFX.playSound("explode",1)
	$Sprite.play("dead")
	$Sprite.scale=Vector2(10,10)
	dead=true