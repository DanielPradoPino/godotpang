extends Area2D

var color
var bubble
var Scale

func _process(delta):
	$Sprite.play("default")
	if $Sprite.frame == $Sprite.frames.get_frame_count("default")-1:
		globals.winds-=1
		$sounds.stop()
		var p=randi()%10
		if p<=5 and p>4:
			bubble=globals.mediumBubble.instance()
			Scale=3
		elif p<=4 and p>2:
			bubble=globals.bigBubble.instance()
			Scale=2.2
		elif p<2:
			bubble=globals.biggerBubble.instance()
			Scale=1
		else:
			bubble=globals.smallBubble.instance()
			Scale=6
		
		bubble.global_position=global_position
		bubble.modulate=color
		bubble.get_node("Sprite").scale=Vector2(bubble.get_node("Sprite").scale.x/(1.5*Scale),bubble.get_node("Sprite").scale.y/(1.5*Scale))
		bubble.get_node("collision").scale=Vector2(bubble.get_node("collision").scale.x/(1.5*Scale),bubble.get_node("collision").scale.x/(1.5*Scale))
		get_node("../../Pumps").add_child(bubble)
		queue_free()

func _ready():
	$sounds.play()
	color=Color(float(randi()%9)/10,float(randi()%9)/10,float(randi()%9)/10,0.8)
	$Sprite.modulate=color