extends AudioStreamPlayer

var sounds
var soundsArray
var sample_library={"balloon":preload("res://assets/sounds/hotairballoon.ogg"),
					"flyby":preload("res://assets/sounds/flyby.ogg")}
	

func playSound(sound,pitch):
	
	if sample_library.has(sound):
		if !is_playing():
			stream = sample_library[sound]
			set_pitch_scale(1)
			play(0.0)