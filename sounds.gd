extends AudioStreamPlayer

var sounds
var soundsArray
var sample_library={"shot":preload("res://assets/sounds/chain.ogg"),
	"hit":preload("res://assets/sounds/platformhit.ogg"),
	"break":preload("res://assets/sounds/platformbreak.ogg")}
	

func playSound(sound,pitch):
	
	if sample_library.has(sound):
		stop()
		stream = sample_library[sound]
		set_pitch_scale(pitch)
		play()
