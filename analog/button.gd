extends Node2D

const INACTIVE_IDX = -1;
export var isDynamicallyShowing = false
export var listenerNodePath = "../../../Player"
export var padname = ""

var ball
var bg 
var animation_player
var parent
var listenerNode
var Event

var centerPoint = Vector2(0,0)
var currentForce = Vector2(0,0)
var halfSize = Vector2()
var ballPos = Vector2()
var squaredHalfSizeLength = 0
var currentPointerIDX = INACTIVE_IDX;

func _ready():
	set_process_input(true)
	bg = get_node("bg")
	animation_player = get_node("AnimationPlayer")
	parent = get_parent()
	halfSize = bg.texture.get_size()/2
	squaredHalfSizeLength = halfSize.x * halfSize.y
	
	if (listenerNodePath != "" && listenerNodePath!=null):
		listenerNodePath = get_node(listenerNodePath)
	elif listenerNodePath=="":
		listenerNodePath = null

#	isDynamicallyShowing = isDynamicallyShowing and parent extends Control
	if isDynamicallyShowing:
		modulate.a = 0
#		hide()

func get_force():
	return currentForce
	

func _input(event):
	if event is InputEventScreenTouch:
		if get_parent().get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			showAtPos(event.global_position)
			sendSignal2Listener()
	if !event.is_pressed():
		reset()

func need2ChangeActivePointer(event): #touch down inside analog	
	if event is InputEventMouseButton or event is InputEventScreenTouch:
		if isDynamicallyShowing:
			#print(get_parent().get_global_rect())
			return get_parent().get_global_rect().has_point(Vector2(event.position.x, event.position.y))
		else:
			var length = (global_position - Vector2(event.position.x, event.position.y)).length_squared();
			return length < squaredHalfSizeLength
	else:
	 	return false

func isActive():
	return currentPointerIDX != INACTIVE_IDX

func extractPointerIdx(event):
	var touch = event is InputEventScreenTouch
	var drag = event is InputEventScreenDrag
	var mouseButton = event is InputEventMouseButton
	var mouseMove = event is InputEventMouseMotion
	
	#print(event)
	if touch or drag:
		return 1
	elif mouseButton or mouseMove:
		#plog("SOMETHING IS VERYWRONG??, I HAVE MOUSE ON TOUCH DEVICE")
		return 0
	else:
		return INACTIVE_IDX
		
func process_input(event):
	calculateForce(event.position.x - global_position.x, event.position.y - global_position.y)
	updateBallPos()
	
	var isReleased = isReleased(event)
	if isReleased:
		reset()


func reset():
	if isDynamicallyShowing:
		hide()
	else:
		updateBallPos()

func showAtPos(pos):
	if isDynamicallyShowing:
		animation_player.play("alpha_in", 0.2)
		global_position = pos
	
func hide():
	animation_player.play("alpha_out", 0.2) 

func updateBallPos():
	pass

func sendSignal2Listener():
	if (listenerNodePath != null):
		listenerNodePath.touch_button_pressed()

func isPressed(event):
	if event is InputEventMouseMotion:
		return (InputEventMouse.button_mask == 1)
	elif event is InputEventScreenTouch:
		return event.is_pressed()

func isReleased(event):
	if event is InputEventScreenTouch:
		return !event.is_pressed()
	elif event is InputEventMouseButton:
		return !event.is_pressed()
