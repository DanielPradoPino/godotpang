extends Node2D
var selectedOption=0
var option=0

func _ready():
	globals.levelCount=1
	get_tree().paused=false
	$menuMusic.play(globals.menuMusicPosition)
	var file=File.new()
	if file.open_encrypted_with_pass(globals.langFile, File.READ,"Nayra") != 0:
		TranslationServer.set_locale("es")
		globals.goto_scene("LanguageMenu")
	else:
		var lang = file.get_line()
		TranslationServer.set_locale(lang)
		file.close()

func _process(delta):
	if Input.is_action_just_pressed("ui_select"):
		$menuSounds.playSound("bounce",1)
		clickOption(option)
	elif Input.is_action_just_pressed("ui_down"):
		$menuSounds.playSound("plop",1)
		option+=1
	elif Input.is_action_just_pressed("ui_up"):		
		$menuSounds.playSound("plop",1)
		option-=1		
	elif Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
	
	if(option>$Options.get_child_count()-1):
		option=0
	if(option<0):
		option=$Options.get_child_count()-1
	for x in range($Options.get_child_count()):
		$Options.get_child(x).modulate=Color("bbabbd")
	$Options.get_child(option).modulate=Color("e47810")

func clickOption(option):
	globals.menuMusicPosition=$menuMusic.get_playback_position()
	if option==0:
		globals.goto_scene("playMenu")
	elif option==1:
		globals.goto_scene("optionsMenu")
	elif option==2:
		get_tree().quit()
	elif option==3:
		globals.goto_scene("howto")


func _input(event):
	if (event is InputEventMouseButton or event is InputEventScreenTouch) and event.is_pressed():
		$menuSounds.playSound("plop",1)
		if $Options/Play/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)) or selectedOption==1:
			clickOption(0)
		elif $Options/Exit/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)) or selectedOption==2:
			clickOption(2)
		elif $Options/Help/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)) or selectedOption==4:
			clickOption(3)
		elif $Options/Options/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)) or selectedOption==3:
			clickOption(1)

func _notification(what):
    if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
        # For Windows
        pass        
    if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: 
        # For android
        get_tree().quit() 
