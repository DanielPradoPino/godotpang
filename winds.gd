extends Area2D


func _process(delta):
	$Sprite.play("default")
	if $Sprite.frame == $Sprite.frames.get_frame_count("default")-1:
		globals.winds-=1
		$sounds.stop()
		queue_free()


func _on_wind_body_entered(body):
	if "Bubble" in body.name:
		body.apply_impulse(Vector2(0,0),Vector2((randi()%300)+200,(randi()%300)+200))

func _ready():
	$sounds.play()