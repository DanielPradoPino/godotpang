extends StaticBody2D

var currentPos=Vector2()
var speed= 2
var data
var pushed=false

func _ready():
	currentPos=global_position
	data=name.split(",")
	
func _process(delta):
	if globals.movePlatforms:
		if "right" in name:
			if global_position.x-currentPos.x<int(data[1]):
				global_position.x+=speed
			else:
				if !globals.onArea:
					name=name.replace("right","left")
		elif "left" in name:
			if currentPos.x-global_position.x<int(data[1]):
				global_position.x-=speed
			else:
				if !globals.onArea:
					name=name.replace("left","right")
	else:
		if global_position.x>currentPos.x:
			global_position.x-=speed
		else:
			global_position.x+=speed

func _on_Portal_body_entered(body):
	if !"block" in body.name:
		globals.movePlatforms=true
		globals.onArea=true
		if !pushed:
			get_node("../../Portal/block13").global_position.y+=25
			pushed=true
			get_node("../../Levelfx").playSound("buttonpush",1)
	
func _on_Portal_body_exited(body):
	if !"Static" in body.name:
		globals.movePlatforms=false
		if pushed:
			get_node("../../Portal/block13").global_position.y-=25
			pushed=false
			get_node("../../Levelfx").playSound("buttonpush",1)
