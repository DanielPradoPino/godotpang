extends AudioStreamPlayer

var sounds
var soundsArray
var sample_library={"plop":preload("res://assets/sounds/plop.ogg"),
				"bounce":preload("res://assets/sounds/bounce.ogg")}
	

func playSound(sound,pitch):
	
	if sample_library.has(sound):
		if sound=="plop":
			stop()
		if !is_playing():
			stream = sample_library[sound]
			set_pitch_scale(1)
			play(0.0)
