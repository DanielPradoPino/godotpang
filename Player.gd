extends KinematicBody2D

const SPEED = 200
const GRAVITY = 20
const JUMP_HEIGHT = -600
const UP = Vector2(0,-1)
const harpoon = preload("res://harpoon.tscn")
const shieldObject = preload("res://shield.tscn")
var collidername="none"
var colliderY=2000
var shield=false
var Shield
var shieldShown=false
var action=""

var motion = Vector2()
var kill = false
var changingScene=false

var canJump=false;
var touchCanShot=0


func showShield():
	if!shieldShown:
		Shield=shieldObject.instance()
		Shield.global_position=global_position
		get_node("../").add_child(Shield)
		shieldShown=true

func _physics_process(delta):
	touchCanShot+=1
	motion.y += GRAVITY
	if !kill:
		if Input.is_action_just_pressed("ui_select"):
			if globals.canShot:
				shotHarpoon()
		elif Input.is_action_pressed("ui_right"):
			right()
		elif Input.is_action_pressed("ui_left"):
			left()
		elif Input.is_action_just_pressed("ui_cancel"):
			globals.pauseMenu()
		else:
			motion.x=0
			$Sprite.play("idle")
		if is_on_floor():
			canJump=true;
		if Input.is_action_just_pressed("ui_up"):
			jump()
		if(get_global_position().y>=100):
			kill=true
		if get_slide_count()>=1:
			if (get_slide_collision(get_slide_count()-1)!=null and !kill):
				var collider = get_slide_collision(get_slide_count()-1).get_collider()
				var col_shape=get_slide_collision(get_slide_count()-1).get_collider_shape_index()
				var collidingNode = globals.getCollisionNode(collider, col_shape)
				if(collidingNode):
					var yDiff=abs(get_global_position().y)-abs(collidingNode.get_global_position().y)
					if(yDiff<=100) and !collider.name==collidername and !colliderY==collider.global_position.y and !colliderY<collider.global_position.y:
						collidername=collider.name
						colliderY=collider.global_position.y
						if is_on_floor():
							motion.y=-300
							canJump=true
					else:
						collidername=collider.name
						colliderY=collider.global_position.y
				
				if "Bubble" in collider.name:
					if !shield:
						if globals.canDie and collider.canDie:
							kill=true
					else:
						shield=false
				else:
					if !globals.canDie and get_node("../").started:
						globals.canDie=true
				if(collider.is_in_group("rigids")):
					collider.apply_impulse(Vector2(0,0),motion)
					get_node("../Level/Level_Sprite/RigidBlock/rigid_sounds").playSound("slide",1)
				elif(collider.is_in_group("doors")):
					if !changingScene:
						changingScene=true
						var Door_Open=get_node("../Level/Level_Sprite/RigidBlock").OPEN
						if Door_Open==true:
							var levelName=get_node("../Level/Level_Sprite/Door").get_child(0).name
							globals.goto_scene(levelName)
			
		if get_node("../Pumps").get_child_count()<=0 and !globals.boss:
			if !get_node("../").finished:
				get_node("../").nextLevel()
		else:
			globals.onScreenBalls=get_node("../Pumps").get_child_count()
			
		globals.shots = get_node("../harpoons").get_child_count()
		if globals.shots<globals.maxShot:
			globals.canShot=true
			
		
	else:
		if !shield:
			$Sprite.play("death")
			$sounds.playSound("dead",1)
			globals.canShot=false
			if $Sprite.frame == $Sprite.frames.get_frame_count("death")-1:
				die()
		else:
			shield=false
			globals.canDie=false
	if shield:
		globals.canDie=false
		showShield()
	else:
		if shieldShown:
			shieldShown=false
			Shield.queue_free()
	if(is_on_floor() and kill):
		pass
	else:
		motion = move_and_slide(motion,UP)
	
func shotHarpoon():
	var Harpoon=harpoon.instance()
	get_parent().get_node("harpoons").add_child(Harpoon)
	Harpoon.global_position.x= global_position.x
	Harpoon.global_position.y= global_position.y+20
	globals.shots+=1
	if globals.shots>=globals.maxShot:
		globals.canShot=false
	
func die():
	kill=false
	$sounds.stop()
	globals.lives-=1
	globals.canDie=false
	get_parent().prepare(false)
	colliderY=2000
	
func jump():
	if canJump:
		motion.y=JUMP_HEIGHT
		$sounds.playSound("jump",1)
		canJump=false

func left():
	motion.x= -SPEED
	$Sprite.flip_h = true
	$Sprite.play("walk")
	if is_on_floor():
		$sounds.playSound("walk",SPEED/150)


func right():
	motion.x = SPEED
	$Sprite.flip_h = false
	$Sprite.play("walk")
	if is_on_floor():
		$sounds.playSound("walk",SPEED/150)
	
func analog_force_change(inForce, inStick):
	if(inStick.get_name()=="Analog"):
		if inForce.x<0:
			action="ui_left"
			Input.action_release("ui_right")
			Input.action_press(action)
		elif inForce.x>0:
			action="ui_right"
			Input.action_release("ui_left")
			Input.action_press(action)
		if inForce.y>0.6:
			action="ui_up"
			Input.action_press(action)
		if inForce==Vector2(0,-0):
			Input.action_release("ui_left")
			Input.action_release("ui_right")
			Input.action_release("ui_up")
	elif(inStick.get_name()=="button"):
		touch_button_pressed()

func touch_button_pressed():
	if touchCanShot>=1:
		Input.action_press("ui_select")
		touchCanShot=0
	
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		# For Windows
		pass        
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: 
		# For android
		globals.levelCount=1
		globals.goto_scene("Menu")
