extends RigidBody2D

var force
var heightSum=1
var local_collision_pos
var canDie=true
var initTime=0
var exeTime=0
var colliderCount=0
var minVel=40+15*globals.handicap

func _ready():
	initTime=get_node("../../").time	
	
func _integrate_forces(state):
	force = (300*globals.handicap) - ((10-mass)*12)
	var lVel=get_linear_velocity()
	if ((lVel.x<0 and lVel.x>=-minVel) or (lVel.x>0 and lVel.x <minVel)):
			lVel.x=lVel.x*globals.handicap
	elif ((lVel.y<0 and lVel.y>=-minVel) or (lVel.y>0 and lVel.y <minVel)):
			lVel.y=lVel.y*globals.handicap
	linear_velocity=lVel
	if heightSum<=1.5:
		heightSum+=0.001
	if position.x<-50 or position.x> get_viewport().size.x+50 or position.y>50 or position.y<((get_viewport().size.y)*-1)-50:
		queue_free()
	if globals.dynamite:
		if !"SmallBubble" in name:
			if initTime-get_node("../..").time>=2:
				die()
		var bigs=0
		for bubbles in get_node("../").get_child_count():
			if !"SmallBubble" in get_node("../").get_child(bubbles).name:
				bigs+=1
		if bigs<=0:
			globals.dynamite=false
	
func die():
	if canDie:
		canDie=false
		$collision.queue_free()
		var Bubble1
		var Bubble2
		var createBalls=true
		var separation=0
		if "BiggerBubble" in name:
			Bubble1=globals.bigBubble.instance()
			Bubble2=globals.bigBubble.instance()
			separation=100
			globals.score+=10*globals.handicap
		elif "BigBubble" in name:
			Bubble1=globals.mediumBubble.instance()
			Bubble2=globals.mediumBubble.instance()
			separation=80
			globals.score+=20*globals.handicap
		elif "MediumBubble" in name:
			Bubble1=globals.smallBubble.instance()
			Bubble2=globals.smallBubble.instance()
			separation=60
			globals.score+=30*globals.handicap
		elif "SmallBubble" in name:
			createBalls=false
			separation=40
			globals.score+=40*globals.handicap
		linear_velocity=Vector2(0,-separation*3)
		if createBalls:
			get_parent().add_child(Bubble2)
			get_parent().add_child(Bubble1)
			Bubble1.modulate=modulate
			Bubble1.global_position.y=global_position.y
			Bubble1.global_position.x=global_position.x
			Bubble1.linear_velocity=Vector2(separation/1.5,-separation)
			Bubble1.mass=mass-2
			Bubble2.modulate=modulate
			Bubble2.global_position.y=global_position.y
			Bubble2.global_position.x=global_position.x
			Bubble2.linear_velocity=Vector2(-separation/1.5,-separation)
			Bubble2.mass=mass-2
			Bubble1.get_node("Sprite").scale=Vector2($Sprite.scale.x/2,$Sprite.scale.y/2)
			Bubble1.get_node("collision").scale=Vector2($collision.scale.x/2,$collision.scale.x/2)
			Bubble1.get_node("Area2D").scale=Vector2($collision.scale.x/2,$collision.scale.x/2)
			Bubble2.get_node("Sprite").scale=Vector2($Sprite.scale.x/2,$Sprite.scale.y/2)
			Bubble2.get_node("collision").scale=Vector2($collision.scale.x/2,$collision.scale.y/2)
			Bubble2.get_node("Area2D").scale=Vector2($collision.scale.x/2,$collision.scale.x/2)
		$Sprite.scale=Vector2($Sprite.scale.x*3,$Sprite.scale.y*3)	
		$Sprite.play("explode")
		$sounds.playSound("plop",1)
		yield(get_tree().create_timer(0.2), "timeout")
		get_node("../../").updateScore()
		if randi() % 10 <=2:
			item()
		queue_free()

func _on_Area2D_body_entered(body):
	if !"Bubble" in body.name:
		var lVel=get_linear_velocity()
		lVel=lVel*heightSum
		if lVel.x<=-force:
			lVel.x=-force
		elif lVel.x>=force:
			lVel.x=force
		elif ((lVel.x<0 and lVel.x>=-50) or (lVel.x>0 and lVel.x <50)):
			lVel.x=lVel.x*2.5*globals.handicap
		elif lVel.y<=-force:
			lVel.y=-force
		elif lVel.y>=force:
			lVel.y=force
		elif ((lVel.y<0 and lVel.y>=-100) or (lVel.y>0 and lVel.y <100)):
			lVel.y=lVel.y*2.5*globals.handicap
		linear_velocity=lVel
		heightSum=1
	
func item():
	if(globals.onscreenItems<globals.maxOnscreenItems):
		globals.onscreenItems+=1
		var item=globals.item.instance()
		item.global_position=global_position
		get_node("../..").add_child(item)