extends Node2D

const boss = preload("res://cadizBoss.tscn")
var Boss

func _ready():
	globals.boss=true
	Boss=boss.instance()
	add_child(Boss)
	Boss.global_position.x=1500
	Boss.global_position.y=-550
	get_parent().time=300
	