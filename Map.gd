extends Node

var city1
var city2
var speed=0.2*globals.speedIncrease
var fadeSpeed=0.005*globals.speedIncrease
var alpha=1
var faded=false
var rampDown=true
var level

func _ready():
	if globals.levelCount==6:
		$Direction.text="Cadiz,LaLinea,LosBarrios,Level6"
	if globals.levelCount==11:
		$Direction.text="Cadiz,LosBarrios,Tarifa,Level11"
	if globals.levelCount==16:
		$Direction.text="Cadiz,Tarifa,LosCanos,Level16"
	if globals.levelCount==21:
		$Direction.text="Cadiz,LosCanos,Cadiz,Level21"
	if globals.levelCount==26:
		$Direction.text="Cadiz,Cadiz,Rota,Level26"
	var data=$Direction.text.split(",")
	level=data[3]
	city1=get_node("%s/%s"%[data[0],data[1]]).global_position
	city2=get_node("%s/%s"%[data[0],data[2]]).global_position
	$Nayra.global_position=city1
	$sounds.playSound("balloon",1)
	

func _process(delta):
	if faded:
		if($Nayra.global_position.x<city2.x):
			$Nayra.global_position.x+=speed
		else:
			$Nayra.global_position.x-=speed
		if($Nayra.global_position.y<city2.y):
			$Nayra.global_position.y+=speed
		else:
			$Nayra.global_position.y-=speed
		if abs(abs($Nayra.global_position.x)-abs(city2.x))<=1:
			if abs(abs($Nayra.global_position.y)-abs(city2.y))<=1:
				if !rampDown:
					$sounds.stop()
					$music.stop()
					globals.goto_scene(level)
				else:
					faded=false
					rampDown=false
	else:
		if rampDown:
			alpha-=fadeSpeed
			if alpha>=0.01:
				$diff.modulate=Color(0,0,0,alpha)
			else:
				faded=true
		else:
			alpha+=fadeSpeed
			if alpha<=0.99:
				$diff.modulate=Color(0,0,0,alpha)
			else:
				faded=true