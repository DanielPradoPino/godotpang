extends Node

var percentage
var maxOccurrences
var data
const wind = preload("res://wind.tscn")
var p

func _ready():
	data=$Label.text.split(",")
	percentage=int(data[0])
	maxOccurrences=int(data[1])
	
func _process(delta):
	p=randi() % 1000
	if p <= percentage:
		if globals.winds< maxOccurrences:
			var Wind=wind.instance()
			globals.winds+=1
			Wind.global_position=Vector2((randi()%700+50),(-1*(randi()%500)-30))
			add_child(Wind)

