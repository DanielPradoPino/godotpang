extends Node2D

var continueExists=false
var file=File.new()
var dir= Directory.new()
var inc=0
func _process(delta):
	inc+=1
	
func _ready():
	$menuMusic.play(globals.menuMusicPosition)
	if file.open_encrypted_with_pass(globals.saveFile, File.READ,"Nayra") != 0:
		continueExists=false
		$Options.get_node("Continue").hide()
	else:
		continueExists=true;
		globals.saveData = file.get_line().split(",")
		$Options.get_node("Continue").show()
		globals.handicap=int(globals.saveData[5])
		$Options/Continue/level.text=globals.saveData[3]
		$Options/Continue/score.text=globals.saveData[1]
		$Options/Continue/lives.text=globals.saveData[2]
		file.close()
	updateHandicap()

func updateHandicap():
	for x in range(5):
		$Options/handicap.get_child(x).show()
	for x in range(5-globals.handicap):
		$Options/handicap.get_child(4-x).hide()
	globals.extraLifeAt*=globals.handicap

func _input(event):
	if (event is InputEventMouseButton or event is InputEventScreenTouch) and event.is_pressed():
		$menuSounds.playSound("plop",1)
		globals.menuMusicPosition=$menuMusic.get_playback_position()
		$menuSounds.playSound("bounce",1)
		if $Options/Play/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			if continueExists:
				dir.remove(globals.saveFile)
			globals.reload()
			globals.goto_scene("Level1")
		elif $Options/Continue/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			if continueExists:
				globals.continueGame()
				globals.score=int(globals.saveData[1])
				globals.lives=int(globals.saveData[2])
				globals.levelCount=int(globals.saveData[3])
				globals.extraLives=globals.score/globals.extraLifeAt
				globals.goto_scene(globals.saveData[0])
		elif $Options/handicapup/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			if globals.handicap<5 and inc>=5:
				globals.handicap+=1
				updateHandicap()
				inc=0
		elif $Options/handicapdown/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			if globals.handicap>1 and inc>=5:
				globals.handicap-=1
				updateHandicap()
				inc=0
		elif $Options/Back/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			globals.menuMusicPosition=$menuMusic.get_playback_position()
			globals.goto_scene("Menu")
			
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		# For Windows
		pass        
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: 
        # For android
		globals.menuMusicPosition=$menuMusic.get_playback_position()
		globals.goto_scene("optionsMenu")

