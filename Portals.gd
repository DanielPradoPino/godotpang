extends Node

var percentage
var maxOccurrences
var maxPortals
var data

const portal = preload("res://portal.tscn")
var p

func _ready():
	data=$Label.text.split(",")
	percentage=int(data[0])
	maxOccurrences=int(data[1])
	maxPortals=int(data[2])
	
func _process(delta):
	if get_node("../").started and !get_node("../").finished:
		if globals.portals<maxPortals:
			p=float(randi() % 1000)+randi()%2
			if p <= percentage:
				if globals.winds< maxOccurrences:
					var Portal=portal.instance()
					globals.winds+=1
					globals.portals+=1
					Portal.global_position=Vector2((randi()%700+50),(-1*(randi()%500)-30))
					add_child(Portal)