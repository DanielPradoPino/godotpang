extends Node2D

func _ready():
	$menuMusic.play(globals.menuMusicPosition)
	if globals.mobile:
		$Options/Graphics.hide()
	
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		globals.goto_scene("Menu")
	
	
func _input(event):
	if (event is InputEventMouseButton or event is InputEventScreenTouch) and event.is_pressed():
		$menuSounds.playSound("plop",1)
		globals.menuMusicPosition=$menuMusic.get_playback_position()
		if $Options/Language/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			globals.goto_scene("LanguageMenu")
		if $Options/Graphics/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			globals.goto_scene("graphics")
		if $Options/Back/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			globals.menuMusicPosition=$menuMusic.get_playback_position()
			globals.goto_scene("Menu")
		if $Options/Player/Control.get_global_rect().has_point(Vector2(event.position.x, event.position.y)):
			globals.goto_scene("playerSelect")
		

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		# For Windows
		pass        
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: 
        # For android
		globals.menuMusicPosition=$menuMusic.get_playback_position()
		globals.goto_scene("Menu")
