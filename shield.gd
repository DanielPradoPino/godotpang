extends Area2D

func _process(delta):
	global_position=get_node("../Player").global_position


func _on_shieldArea_body_entered(body):
	if "Bubble" in body.name:
		if body.canDie:
			body.die()
			get_node("../Player").shield=false