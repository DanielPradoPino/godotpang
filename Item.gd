extends Area2D

var speed=5
var motion=true
var createTime=0
var enabledTime=0

func _ready():
	createTime=get_node("../").time
	var rnd = randi() % 10
	if rnd<=6 and rnd>3:
		if !get_node("../Player").shield:
			$Desc.text="shield"
			get_node("2shots").hide()
			$coin1.hide()
			$dynamite.hide()
			$shield.show()
		else:
			queue_free()
	if rnd<=3 and rnd>1:
		$Desc.text="coin1"
		get_node("2shots").hide()
		$coin1.show()
		$dynamite.hide()
		$shield.hide()
	if rnd<=1:
		if !globals.dynamite:
			$Desc.text="dynamite"
			get_node("2shots").hide()
			$coin1.hide()
			$dynamite.show()
			$shield.hide()
		else:
			queue_free()
	else:
		if globals.maxShot>=2:
			queue_free()
		
func _process(delta):
	if motion:
		global_position.y+=speed
	if createTime-get_node("../").time>=5:
		if visible:
			hide()
		else:
			show()
	if createTime-get_node("../").time>=10:
		globals.onscreenItems-=1
		queue_free()

func _on_Item_body_entered(body):
	if(body.name=="Boundaries" or body.is_in_group("platforms")):
		motion=false
	if(body.name=="Player"):
		if $Desc.text=="2shots":
			globals.maxShot=2
			get_node("..").Header.get_node("Items/shots").show()
			get_node("../Player/sounds").playSound("ohyeah",1.5)
		elif $Desc.text=="dynamite":
			globals.dynamite=true
		elif $Desc.text=="coin1":
			globals.score+=100
			get_node("../Player/sounds").playSound("ohyeah",1.5)
		elif $Desc.text=="shield":
			get_node("../Player").shield=true
		get_node("../").updateScore()
		globals.onscreenItems-=1
		queue_free()

func _on_Item_body_exited(body):
	if(body.name=="Boundaries" or body.is_in_group("platforms")):
		motion=true
