extends Node

var language="es"
var toOptions=false
var file=File.new()
var es=false
var counter=0

func _process(delta):
	counter+=1
	
func _ready():
	$menuMusic.play(globals.menuMusicPosition)
	if file.open_encrypted_with_pass(globals.langFile, File.READ,"Nayra") != 0:
		toOptions=false
	else:
		language = file.get_line()
		if language=="es":
			es=true
		else:
			es=false
		changeLang()
		file.close()
		toOptions=true

func _input(event):
	if (event is InputEventMouseButton or event is InputEventScreenTouch):
		if $CanvasLayer/left.get_global_rect().has_point(Vector2(event.position.x, event.position.y)) and event.is_pressed():
			if counter>=30:
				$menuSounds.playSound("plop",1)
				es = not es
				counter=0
				changeLang()
		elif $CanvasLayer/button.get_global_rect().has_point(Vector2(event.position.x, event.position.y)) and event.is_pressed():
			$menuSounds.playSound("plop",1)
			_on_Button_pressed()
		

func changeLang():
	if !es:
		$BGSpain.hide()
		$BGUK.show()
		TranslationServer.set_locale("en")
		language="en"
	else:
		$BGSpain.show()
		$BGUK.hide()
		TranslationServer.set_locale("es")
		language="es"

func _on_Button_pressed():
	$menuSounds.playSound("bounce",1)
	var data = "%s"%language
	if file.open_encrypted_with_pass(globals.langFile, File.WRITE,"Nayra") != 0:
		print("Error saving data")
	else:
		file.store_line(data)
		file.close()
	globals.menuMusicPosition=$menuMusic.get_playback_position()
	if !toOptions:
		globals.goto_scene("Menu")
	else:
		globals.goto_scene("optionsMenu")
		
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		# For Windows
		pass        
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: 
		# For android
		globals.menuMusicPosition=$menuMusic.get_playback_position()
		if !toOptions:
			globals.goto_scene("Menu")
		else:
			globals.goto_scene("optionsMenu")
