extends Node


var bubbles
var Player
var Analog
var pumps
var Header
var time=3
var timer
var readyToGo=false
var readyCountFinish=false
var ready
var finished=false
var started=false
var level_name
var fromDead=false
var movingPlatforms=[0,12,16,17,18,22]


func _ready():
	var data = "%s,%d,%d,%d,%s,%d"%[globals.level,globals.score,globals.lives,globals.levelCount,globals.playerName,globals.handicap]
	var file = File.new()
	if file.open_encrypted_with_pass(globals.saveFile, File.WRITE,"Nayra") != 0:
		print("Error saving data")
	else:
		file.store_line(data)
		file.close()		
	randomize()
	prepare(true)

func prepare(init):
	if init:
		timer = Timer.new()
		timer.set_one_shot(true)
		timer.set_wait_time(1)
		timer.connect("timeout",self,"TimeDecrease")
		add_child(timer)
		timer.start()
		timer.pause_mode=2
		Header=globals.header.instance()
		Header.global_position= Vector2(0,-610)
		Header.get_node("Level").text=str(globals.levelCount)
		get_node("./").add_child(Header)
		globals.shots=0
		globals.onscreenItems=0
		globals.maxShot=1
		globals.dynamite=false
		globals.map=false
		globals.cutScn=false
		globals.winds=0
		globals.portals=0
		globals.onArea=false
		if globals.levelCount in movingPlatforms:
			globals.movePlatforms=true
		else:
			globals.movePlatforms=false
		Player=globals.player.instance()
		Player.global_position=$Marks/Player/start.global_position
		add_child(Player)
		if globals.mobile:
			Analog=globals.analog.instance()
			add_child(Analog)
			globals.speedIncrease=1.7
		
	else:
		if !globals.lives<=0:
			fromDead=true
			updateLives()
			get_tree().paused=true
			globals.canShot=true
		else:
			globals.levelCount=1
			globals.goto_scene("Menu")
	time=3
	readyToGo=false
	readyCountFinish=false
	finished=false
	started=false
	ready=globals.readyScn.instance()
	ready.global_position= Vector2(512,-300)
	get_node("./").add_child(ready)
	$Levelfx.playSound("ready",1)
	updateScore()
	updateLives()
	
func updateLives():
	if globals.lives>=1:
		Header.get_node("Lives/Lives").text="X%d"%(globals.lives-1)
		if (globals.lives==2):
			Header.get_node("Lives/Lives").modulate=Color("fffb00")
		elif(globals.lives<=1):
			Header.get_node("Lives/Lives").modulate=Color("ff0202")
		else:
			Header.get_node("Lives/Lives").modulate=Color("38fe00")
			
func updateScore():
	Header.get_node("score").text=str(globals.score)
	if (globals.score/globals.extraLifeAt) > globals.extraLives:
		$Levelfx.playSound("extraLive",1)
		globals.lives+=1
		globals.extraLives+=1
		updateLives()
	



func start():
	#bubbles creation
	var Bubbles=$Marks/Bubbles.get_child_count()
	var scale=1
	for Bubble in Bubbles:
		var data=get_node("Marks/Bubbles").get_child(Bubble).name.split(",")
		if(data[0]=="bigger"):
			bubbles=globals.biggerBubble.instance()
			scale=1
		elif(data[0]=="big"):
			bubbles=globals.bigBubble.instance()
			scale=2.2
		elif(data[0]=="medium"):
			bubbles=globals.mediumBubble.instance()
			scale=3
		elif(data[0]=="small"):
			bubbles=globals.smallBubble.instance()
			scale=6
		$Pumps.add_child(bubbles)
		bubbles.linear_velocity=Vector2((randi() % 100)+100,100)
		bubbles.global_position=get_node("Marks/Bubbles").get_child(Bubble).global_position
		bubbles.modulate=Color(data[1])
		bubbles.get_node("Sprite").scale=Vector2(bubbles.get_node("Sprite").scale.x/(1.8*scale),bubbles.get_node("Sprite").scale.y/(1.8*scale))
		bubbles.get_node("collision").scale=Vector2(bubbles.get_node("collision").scale.x/(1.8*scale),bubbles.get_node("collision").scale.x/(1.8*scale))
		bubbles.get_node("Area2D").scale=Vector2(bubbles.get_node("Area2D").scale.x/(1.8*scale),bubbles.get_node("Area2D").scale.x/(1.8*scale))

func TimeDecrease():
	time-=1
	if readyToGo:
		if!finished:
			Header.get_node("time").text=str(time)
			if time<=0:
				$Player.kill=true
			timer.start()
		else:
			if time<=0:
				globals.goto_scene(level_name)
			else:
				timer.start()
	else:
		ready.get_node("time").text=str(time)
		if !readyCountFinish:
			$Levelfx.playSound("timeping",1)
			if time<=0:
				time=1
				ready.get_node("time").visible=false
				ready.get_node("goText").visible=true
				readyCountFinish=true
				$Levelfx.playSound("letsgo",1)
		else:
			ready.free()
			time=100
			if globals.boss:
				time=300
			readyToGo=true
			get_node("./Player")
			get_tree().paused=false
			if !fromDead:
				start()
			globals.canDie=true
			started=true
		timer.start()
	

func nextLevel():
	if started:
		globals.levelCount+=1
		level_name="Level%d"%globals.levelCount
		$Decals/youwin.show()
		$Decals/shadowed.show()
		$Levelsounds.stop()
		$Levelfx.playSound("leveldone",1)
		var toAdd=time
		time=5
		finished=true
		for x in range(toAdd):
			globals.score+=5
			updateScore()
			Header.get_node("time").text=str(toAdd-x-1)
			yield(get_tree().create_timer(0.01), "timeout")
		